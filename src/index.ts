const LocalPlayer = game.GetService("Players").LocalPlayer;

/**
 * Whether or not this script is running on the server.
 */
export const SERVER = game.GetService("RunService").IsServer();

/**
 * Whether or not this script is running on the client.
 */
export const CLIENT = !SERVER;

/**
 * A connection that can be disconnected.
 */
export class Connection {
  constructor(private onDisconnect: () => void) {}

  /**
   * Disconnects the `Connection`.
   */
  public disconnect() {
    this.onDisconnect();
  }
}

/**
 * A hook that a number of functions can bind to.
 *
 * When invoked, the hook calls all of the functions in a non-deterministic
 * manner, returning the first non-`undefined` value produced, if any.
 */
export class Hook<T extends Array<any>, R> {
  private fns: Set<(...v: T) => R | void> = new Set();

  /**
   * Binds a function to the `Hook`.
   */
  public add(fn: (...v: T) => R | void) {
    this.fns.add(fn);

    return new Connection(() => {
      this.fns.delete(fn);
    });
  }

  /**
   * Invokes the `Hook`, returning the first non-`undefined` value encountered,
   * if any.
   */
  public call(...arg: T) {
    for (const fn of this.fns.values()) {
      const res = fn(...arg);

      if (res !== undefined) {
        return res;
      }
    }
  }
}

type check<T> = (value: unknown) => value is T;
type getCheckType<Q> = Q extends check<infer T> ? T : never;

/**
 * A named `Message` that can send and validate data across the client-server
 * boundary. Works well with `@rbxts/t`.
 *
 * Sending and constructing `Message`s never blocks, regardless of whether the
 * `RemoteEvent` exists or the receiver is ready.
 *
 * Messages are created within the `ModuleScript` hosting `hook`, which should
 * always be placed within `ReplicatedStorage`.
 *
 * Data that cannot be validated is discarded automatically.
 *
 * ## Limitations
 *
 * `Message`s only support one argument at the moment. All other arguments
 * received from the inner `RemoteEvent` are discarded. You can use `t.array`
 * to sidestep this issue.
 *
 * `t` does not currently support several validation options in TypeScript,
 * because these options are mostly incompatible with type inference. In some
 * circumstances, you may need to roll your own. In this case, `Message` will
 * accept any type guard as a validation function: `(value) => value is T`.
 */
export class Message<T extends check<unknown>> {
  private fns = new Set<(ply: Player, msg: getCheckType<T>) => void>();
  private remote?: RemoteEvent;

  /**
   * Construct a new `Message` with a unique name and a validation function.
   */
  constructor(private name: string, private validator: T) {
    if (SERVER) {
      const remote = (this.remote = new Instance("RemoteEvent"));

      remote.Name = name;
      remote.Parent = script;

      remote.OnServerEvent.Connect((ply, msg) => {
        if (validator(msg)) {
          for (const fn of this.fns.values()) {
            fn(ply, msg as getCheckType<T>);
          }
        }
      });
    } else {
      spawn(() => {
        const remote = (this.remote = script.WaitForChild(name) as RemoteEvent);

        remote.OnClientEvent.Connect(msg => {
          if (validator(msg)) {
            for (const fn of this.fns.values()) {
              fn(LocalPlayer, msg as getCheckType<T>);
            }
          }
        });
      });
    }
  }

  /**
   * Binds a function to the `Message`. The function will be invoked when valid
   * data is received. Invalid data will be discarded.
   *
   * On the client, `ply` is always the local player.
   */
  public connect(fn: (ply: Player, msg: getCheckType<T>) => void) {
    this.fns.add(fn);

    return new Connection(() => {
      this.fns.delete(fn);
    });
  }

  /**
   * Sends a `Message` over the client-server boundary. If the remote is not
   * yet set up, this is a no-op.
   */
  public send(msg: getCheckType<T>) {
    if (!this.remote) {
      return;
    }

    if (SERVER) {
      this.remote.FireAllClients(msg);
    } else {
      this.remote.FireServer(msg);
    }
  }

  /**
   * Sends a `Message` to a particular client.
   */
  public sendToPlayer(ply: Player, msg: getCheckType<T>) {
    if (!this.remote) {
      return;
    }

    this.remote.FireClient(ply, msg);
  }

  /**
   * Sends a `Message` to a set of clients.
   */
  public sendToPlayers(plys: Array<Player>, msg: getCheckType<T>) {
    if (!this.remote) {
      return;
    }

    for (const [_, ply] of plys.entries()) {
      this.remote.FireClient(ply, msg);
    }
  }
}
